import urllib2
import sys, getopt
import os

options,remainder = getopt.gnu_getopt(sys.argv[1:],'l:',["link="])
cwd = current_dir = dir_path = os.path.dirname(os.path.abspath(__file__))

tempFilePath = cwd+"/last_ip/last_ip.txt"
serverFilePath = cwd+"/server_ip/server_ip.txt"

server = ""

## READ FROM PARAMETER
for opt,arg in options:
    if opt in ("-l","--link"):
        server = arg


## READ FROM FILE
if(server == ""):
    server = open(serverFilePath,"r").read()

print("IP IS "+server)

ip = urllib2.urlopen(server).read()
try:
    file = open(tempFilePath,"r")
except Exception as e:
    file = open(tempFilePath, "wr+")
    print(e)
lastIp = file.read()
file.close()

if(lastIp != ip):
    print("RESET")
    os.system("supervisorctl restart all")
else:
    print("YOU IP LOOKS FINE GO ON")

print("------------------------------------------------")
print("YOUR IP WAS => " + lastIp)
print("NOW YOUR IP IS => " + ip)
print("------------------------------------------------")

file = open(tempFilePath,"w+")
file.write(ip)
file.close()